# cURL

Tutoriel - prendre cURL en main.

# Prérequis


*  Avoir la bibliothéque cURL de PHP installer sur sa machine

Pour ubuntu :

    sudo apt install php-curl

*  cloner le dépôt en local

Avec SSH :

    git clone git@gitlab.com:Benji_B/curl.git

Avec HTTP :

    git clone https://gitlab.com/Benji_B/curl.git

# Tuto

Le but est de récupérer le contenu html de cette [page](https://jsonformatter.curiousconcept.com/) et de le parser afin de ne faire apparaître dans notre navigateur que le contenu de la partie *'About'* de la page.

Une fois le dépôt cloner sur votre machine,
vous trouverez dans le dossier 'curl' un fichiers **about.php**.

Ce script contient une solution (parmis beaucoup d'autres!) qui permet d'obtenir le résultat voulu grâce à plusieurs fonctions de la bibliothèque cURL tel que `curl_init();` ainsi que d'autres fonctions php pour le parsage du contenu tel que `explode()`.

Vous avez toutes les clés en main, je vous conseille d'essayer de le faire sans regarder la solution. Allez consulter la doc sur officielle sur [cURL](https://www.php.net/manual/fr/book.curl.php) afin de comprendre chaque fonction.
J'ai également commenté le code pas à pas si vous êtes bloqué.

Une fois votre script terminer, vous pouvez:

- Placer le script dans votre environnement apache (/var/www/) et le lancer via votre navigateur (localhost/) afin de vérifier le contenu de votre récupération cURL.

ou

- Executer le script via le terminal grâce à la commande suivante:

    `php -f 'votre_script.php'`

Bon courage à vous.
