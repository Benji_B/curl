<?php
/**
 * On initialise une session curl dans une variable.
 */
$init = curl_init();


/**
 * on défini les différentes options de transmission que cURL enverra au serveur.
 * @param string CURLOPT_URL : l'url de destination, la ou on veux pointer.
 * @param bool CURLOPT_RETURNTRANSFER : récupère la réponse du serveur.
 */
$options = array(
	CURLOPT_URL => "https://jsonformatter.curiousconcept.com/",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_SSL_VERIFYHOST => false,
	CURLOPT_SSL_VERIFYPEER => false
);
curl_setopt_array($init, $options);

/**
 * on execute la session cURL précédemment configuré
 * @param string $output : récupére le contenu html de la réponse serveur.
 */
$output = curl_exec($init);

/**
 * On parse le contenu récupérer afin de ne faire apparaitre que le contenu de la partie 'About'
 */
$output = explode("class=\"about\">", $output);
$output = explode("<section class=\"learn\">", $output[1]);

/**
 * Enfin on affiche $output et on ferme la session cURL.
 */
echo "<section class=\"about\">".$output[0];
curl_close($init);

?>
